# DRF auth

Django rest framework authentication system.

## Features

- Registration
- Login
- Logout
- Users detail list
- Single user detail
- User update detail
- Delete user
- Change password
- Check password

## Getting started

```
$ git clone https://github.com/AmirAhrari/drf-auth.git && cd drf-auth
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

> server has been started in http://localhost:8000/

## Endpoints

### Registration

Default URL: api/auth/register/ <br>
Method: POST <br>
Inputs: username, email, password <br>
Permissions: - <br>
Response: id, username, email, password, token

### Login

Default URL: api/auth/login/ <br>
Method: POST <br>
Inputs: username, password <br>
Permissions: - <br>
Response: user, token

### Logout

Default URL: api/auth/logout/ <br>
Method: POST <br>
Inputs: - <br>
Permissions: IsAuthenticated <br>
Response: HTTP_200_OK

### Users detail list

Default URL: api/auth/users/ <br>
Method: GET <br>
Inputs: - <br>
Permissions: - <br>
Response: users list (id, username, email)

### Single user detail

Default URL: api/auth/users/<user_id>/ OR api/auth/users/self/ <br>
Method: GET <br>
Inputs: - <br>
Permissions: - <br>
Response: id, username, email

### User update detail

Default URL: api/auth/users/<user_id>/ OR api/auth/users/self/ <br>
Method: PUT, PATCH <br>
Inputs: username, email <br>
Permissions: CurrentUserOrAdmin <br>
Response: id, username, email

### Delete user

Default URL: api/auth/users/<user_id>/ OR api/auth/users/self/ <br>
Method: DELETE <br>
Inputs: - <br>
Permissions: CurrentUserOrAdmin <br>
Response: HTTP_204_NO_CONTENT

### Change password

Default URL: api/auth/change-password/ <br>
Method: POST <br>
Inputs: old_password, new_password <br>
Permissions: IsAuthenticated <br>
Response: HTTP_200_OK

### Check password

Default URL: api/auth/check-password/ <br>
Method: POST <br>
Inputs: password <br>
Permissions: IsAuthenticated <br>
Response: HTTP_200_OK

## Todos

- Reset password
